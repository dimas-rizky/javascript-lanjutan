function validateForm() {
    const name =
        document.getElementById("name").value;
    const username =
        document.getElementById("username").value;
    const email =
        document.getElementById("email").value;
    const password =
        document.getElementById("password").value;
    const subject =
        document.getElementById("subject").value;

    const nameError =
        document.getElementById("name-error");
    const usernameError = document.getElementById(
        "username-error"
    );
    const emailError = document.getElementById(
        "email-error"
    );
    const passwordError = document.getElementById(
        "password-error"
    );
    const subjectError = document.getElementById(
        "subject-error"
    );

    nameError.textContent = "";
    usernameError.textContent = "";
    emailError.textContent = "";
    passwordError.textContent = "";
    subjectError.textContent = "";

    let isValid = true;

    if (name === "" || /\d/.test(name)) {
        nameError.textContent =
            "Masukkan nama Anda dengan benar.";
        isValid = false;
    }

    if (username === "") {
        usernameError.textContent =
            "Masukkan nama pengguna Anda.";
        isValid = false;
    }

    if (email === "" || !email.includes("@")) {
        emailError.textContent =
            "Masukkan email yang valid.";
        isValid = false;
    }

    if (password === "" || password.length < 6) {
        passwordError.textContent =
            "Password minimal 6 Karakter.";
        isValid = false;
    }

    if (subject === "") {
        subjectError.textContent =
            "Silahkan Pilih gender anda.";
        isValid = false;
    }
    
    if (isValid) {
        alert("Form berhasil dikirim!");
    }
    
    return isValid;
}
